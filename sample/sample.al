class Counter {
  int a;

  def _inc_() {
    a + 1;
  }

  class Human {
    int name;
  }
}
