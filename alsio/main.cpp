﻿/***********************************************************************//**
	@file
***************************************************************************/
#include "alsio/Compiler.hpp"
/***********************************************************************//**
	@brief 
***************************************************************************/
int main(int argc, const char** argv) {
  alsio::Compiler compiler;
  compiler.compile();
  return 0;
}
/***********************************************************************//**
	$Id$
***************************************************************************/
