﻿/***********************************************************************//**
	@file
***************************************************************************/
#pragma once

namespace alsio {
/***********************************************************************//**
	@brief 
***************************************************************************/
enum Opecode {
  OPECODE_NOP, 
  OPECODE_HALT, 
  OPECODE_PUSH_Q, 
  OPECODE_PUSH_INT, 
  OPECODE_PUSH_FLOAT, 
  OPECODE_ADD_INT, 
  OPECODE_ADD_FLOAT, 
  OPECODE_SUB_INT, 
  OPECODE_SUB_FLOAT, 
  OPECODE_MUL_INT, 
  OPECODE_MUL_FLOAT, 
  OPECODE_DIV_INT, 
  OPECODE_DIV_FLOAT, 
  OPECODE_MAX
};
/***********************************************************************//**
	$Id$
***************************************************************************/
}
