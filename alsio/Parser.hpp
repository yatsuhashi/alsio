﻿/***********************************************************************//**
	@file
***************************************************************************/
#pragma once

#include "alsio/Variant.hpp"
#include "alsio/ast/BlockNode.hpp"
#include "alsio/ast/CallNode.hpp"
#include "alsio/ast/ConstExprNode.hpp"
#include "alsio/ast/DefKlassNode.hpp"
#include "alsio/ast/DefVarNode.hpp"
#include "alsio/ast/ExprNode.hpp"
#include "alsio/ast/FuncNode.hpp"
#include "alsio/ast/KlassPathNode.hpp"
#include "alsio/ast/KlassRefNode.hpp"
#include "alsio/ast/LamdaKlassNode.hpp"
#include "alsio/ast/LamdaNode.hpp"
#include "alsio/ast/ListNode.hpp"
#include "alsio/ast/MethodNode.hpp"
#include "alsio/ast/Node.hpp"
#include "alsio/ast/ParamListNode.hpp"
#include "alsio/ast/ParamNode.hpp"
#include "alsio/ast/ReturnNode.hpp"
#include "alsio/ast/SymbolNode.hpp"

#include "alsio/Parser.cpg.hpp"
/***********************************************************************//**
	$Id$
***************************************************************************/
