﻿/***********************************************************************//**
	@file
***************************************************************************/
#pragma once

namespace alsio {
/***********************************************************************//**
	@brief 任意値
***************************************************************************/
class Variant {
 private:
  const std::type_info* type_;
  std::shared_ptr<void> value_;

 public:
  /**
   * デフォルトコンストラクタ
   */
  Variant() : type_(nullptr) {}

  /**
   * コンストラクタ
   * @param[in] value インスタンスのポインタ
   */
  template <class T>
  Variant(const std::shared_ptr<T>& value) {
    type_ = &typeid(T);
    value_ = value;
  }

  /**
   * コンストラクタ
   * @param[in] value インスタンス
   */
  template <class T>
  Variant(const T& value) : Variant(std::make_shared<T>(value)) {}

  /**
   * コンストラクタ
   * @param[in] value 文字列のポインタ
   */
  Variant(const char* value) : Variant(std::string(value)) {}

  /**
   * コンストラクタ
   * @param[in] value 文字列のポインタ
   */
  Variant(char* value) : Variant(std::string(value)) {}

  /**
   * コピーコンストラクタ
   */
  Variant(const Variant& src) = default;

  /**
   * デストラクタ
   */
  ~Variant() = default;

  /**
   * インスタンスのポインタを取得する
   * @return インスタンスのポインタ
   */
  template <class T>
  std::shared_ptr<T> get() const {
    assert(isA<T>());
    return std::static_pointer_cast<T>(value_);
  }
  /**
   * インスタンスを取得する
   * @return インスタンス
   */
  template <class T>
  const T& as() const {
    return *get<T>();
  }

  bool isNull() const {
    return !type_;
  }

  template <class T>
  bool isA() const {
    return !isNull() && *type_ == typeid(T);
  }

  std::string toString() const;
};
/***********************************************************************//**
	$Id$
***************************************************************************/
}
