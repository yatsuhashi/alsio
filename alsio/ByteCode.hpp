﻿/***********************************************************************//**
	@file
***************************************************************************/
#pragma once

namespace alsio {
/***********************************************************************//**
	@brief 
***************************************************************************/
union ByteCode {
  struct {
    uint16_t opecode;
    int16_t operand;
  };
  int_t i;
  float_t f;
  symbol_t s;
};
static_assert(sizeof(ByteCode) == 4, "ByteCode is illegal size");
/***********************************************************************//**
	$Id$
***************************************************************************/
}
