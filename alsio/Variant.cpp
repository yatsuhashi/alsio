﻿/***********************************************************************//**
	@file
***************************************************************************/
#include "alsio/Compiler.hpp"
#include "alsio/Symbolizer.hpp"
#include "alsio/Variant.hpp"

namespace alsio {
/***********************************************************************//**
	@brief 文字列に変換する
	@return 文字列
***************************************************************************/
std::string Variant::toString() const {
  std::stringstream stream;
  if(isNull()) {
    stream << "(null)";
  }
  else if(isA<int_t>()) {
    stream << as<int_t>();
  }
  else if(isA<float_t>()) {
    stream << as<float_t>();
  }
  else if(isA<bool>()) {
    stream << (as<bool>() ? "true" : "false");
  }
  else if(isA<symbol_t>()) {
    stream << ":" << as<symbol_t>();
  }
  else if(isA<std::string>()) {
    stream << "\"" << as<std::string>() << "\"";
  }
  else {
    stream << "?";
  }
  return stream.str();
}
/***********************************************************************//**
	$Id$
***************************************************************************/
}
