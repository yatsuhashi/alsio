﻿/***********************************************************************//**
	@file
***************************************************************************/
#pragma once

#if !defined(yyFlexLexerOnce)
#include <FlexLexer.h>
#endif

#include "alsio/Variant.hpp"

namespace alsio {
/***********************************************************************//**
	@brief 
***************************************************************************/
class Scanner
  : public yyFlexLexer
{
  typedef yyFlexLexer super;

 private:
  Compiler& compiler_;
  Variant value_;

 public:
  Scanner(Compiler& compiler);
  ~Scanner() = default;

  Compiler& getCompiler() const {
    return compiler_;
  }

  int yylex();

  const Variant& getValue() const {
    return value_;
  }

 protected:
  template <class T>
  void setValue(const T& value) {
    value_ = Variant(value);
  }
};
/***********************************************************************//**
	$Id$
***************************************************************************/
}
