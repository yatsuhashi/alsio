﻿/***********************************************************************//**
	@file
***************************************************************************/
#include "alsio/Symbolizer.hpp"

namespace alsio {
/***********************************************************************//**
	@brief コンストラクタ
***************************************************************************/
Symbolizer::Symbolizer()
  : symbol_(0)
{
}
/***********************************************************************//**
	@brief 
***************************************************************************/
symbol_t Symbolizer::getSymbol(const std::string& label) {
  auto iter = indexes_.find(label);
  if(iter != indexes_.end()) {
    return iter->second;
  }
  indexes_[label] = ++symbol_;
  return symbol_;
}
/***********************************************************************//**
	@brief 
***************************************************************************/
std::string Symbolizer::getLabel(symbol_t symbol) const {
  auto iter = std::find_if(indexes_.begin(), 
                           indexes_.end(), 
                           [&](const Index::value_type& value) {
                             return value.second == symbol;
                           });
  return (iter != indexes_.end()) ? iter->first : std::string();
}
/***********************************************************************//**
	$Id$
***************************************************************************/
}
