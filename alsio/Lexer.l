%{
#include "alsio/Parser.hpp"
#include "alsio/Scanner.hpp"
%}

%x COMMENT

%option noyywrap
%option yylineno
%option c++
%option yyclass="alsio::Scanner"

%%
<*>[[:space:]]+

<*>"/*"		{ BEGIN(COMMENT); }
<COMMENT>"*/"	{ BEGIN(INITIAL); }
<COMMENT>.+

<*>"//".*

"class"		{ return token_CLASS; }
"def"		{ return token_DEF; }
"dump"		{ return token_DUMP; }
"end"		{ return token_END; }
"func"		{ return token_FUNC; }
"return"	{ return token_RETURN; }

"void"|"int"|"float"|"bool" {
	setValue(yytext);
	return token_KLASS;
}

"true" {
	setValue(true);
	return token_NUMBER;
}
"false" {
	setValue(false);
	return token_NUMBER;
}

"::"		{ return token_SEP; }
"->"		{ return token_ARROW; }

"++"		{ return token_INC; }
"--"		{ return token_DEC; }

"<="		{ return token_CMP_LE; }
">="		{ return token_CMP_GE; }
"=="		{ return token_CMP_EQ; }
"!="		{ return token_CMP_NE; }
"<"		{ return token_CMP_LT; }
">"		{ return token_CMP_GT; }

"+"		{ return token_ADD; }
"-"		{ return token_SUB; }
"*"		{ return token_MUL; }
"/"		{ return token_DIV; }
"%"		{ return token_MOD; }

"("		{ return token_PAREN_L; }
")"		{ return token_PAREN_R; }
"{"		{ return token_CURLY_L; }
"}"		{ return token_CURLY_R; }

"="		{ return token_EQ; }

","		{ return token_COMMA; }
":"		{ return token_COLON; }
";"		{ return token_SEMICOLON; }

(\"[^\"]*\")|(\'[^\']*\') {
	setValue(std::string(&yytext[1], strlen(yytext) - 2));
	return token_STRING;
}

([[:digit:]]+)|(0x[[:xdigit:]]+) {
	setValue(int_t(strtol(yytext, nullptr, 0)));
	return token_NUMBER;
}
[[:digit:]]+"."[[:digit:]]+ {
	setValue(float_t(strtod(yytext, nullptr)));
	return token_NUMBER;
}

:[[:alnum:]_]+ {
	setValue(&yytext[1]);
        return token_NUMBER;
}

[[:upper:]][[:upper:]_]* {
	setValue(yytext);
	return token_CONST;
}

[[:upper:]][[:alnum:]_]+ {
	setValue(yytext);
	return token_KLASS;
}

[[:lower:]_][[:alnum:]_]* {
	setValue(yytext);
	return token_SYMBOL;
}

%%
