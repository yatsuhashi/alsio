﻿/***********************************************************************//**
	@file
***************************************************************************/
#include "alsio/Compiler.hpp"
#include "alsio/Klass.hpp"
#include "alsio/Parser.hpp"
#include "alsio/Scanner.hpp"
#include "alsio/Symbolizer.hpp"

namespace alsio {
/***********************************************************************//**
	@brief コンストラクタ
***************************************************************************/
Compiler::Compiler()
  : scanner_(new Scanner(*this)), 
    symbolizer_(new Symbolizer())
{
}
/***********************************************************************//**
	@brief デストラクタ
***************************************************************************/
Compiler::~Compiler() {
}
/***********************************************************************//**
	@brief コンパイル
***************************************************************************/
void Compiler::compile() {
  Parser<std::shared_ptr<Node>, Compiler> parser(*this);
  while(true) {
    source_.setLine(scanner_->lineno());
    auto token = static_cast<Token>(scanner_->yylex());
    auto value = createNode<ConstExprNode>(scanner_->getValue());
    std::cout <<
      token_label(token) <<
      "(" << value->toString() << ")" << 
      std::endl;
    source_.setLine(scanner_->lineno());
    if(parser.post(token, value)) {
      break;
    }
  }
  if(!parser.error()) {
    std::shared_ptr<Node> value;
    if(parser.accept(value)) {
      std::cerr << "accepted:" << std::endl;
      value->dump(std::cerr);
    }
  }
}
/***********************************************************************//**
	@brief 
***************************************************************************/
void Compiler::syntax_error() {
  std::cerr << "syntax error at " << source_.toString() << std::endl;
}
/***********************************************************************//**
	@brief 
***************************************************************************/
void Compiler::stack_overflow() {
  std::cerr << "stack overflow at" << source_.toString() << std::endl;
}
/***********************************************************************//**
	@brief 文字列にダウンキャストする
	@param[out] dst 出力
	@param[in] src 入力
***************************************************************************/
void Compiler::downcast(std::string& dst, 
                        const std::shared_ptr<Node>& src) {
  auto node = std::dynamic_pointer_cast<ConstExprNode>(src);
  assert(node);
  dst = node->getValue().as<std::string>();
}
/***********************************************************************//**
	@brief ブロックにステートメントを追加する
	@param[in] block ブロック
	@param[in] statement ステートメント
	@return ブロック
***************************************************************************/
std::shared_ptr<BlockNode>
Compiler::appendStatement(std::shared_ptr<BlockNode> block, 
                          std::shared_ptr<Node> statement) {
  block->appendChild(statement);
  return block;
}
/***********************************************************************//**
	@brief 仮引数を追加する
	@param[in] params 仮引数リスト
	@param[in] param 仮引数
	@return 仮引数リストノード
***************************************************************************/
std::shared_ptr<ParamListNode>
Compiler::appendParam(std::shared_ptr<ParamListNode> params, 
                      std::shared_ptr<ParamNode> param) {
  params->appendParam(param);
  return params;
}
/***********************************************************************//**
	@brief クラスパスを生成する
	@param[in] name クラス名
	@return クラスパス
***************************************************************************/
std::shared_ptr<KlassPathNode>
Compiler::createKlassPath(const std::string& name) {
  auto klassPath = createNode<KlassPathNode>();
  klassPath->appendPath(name);
  return klassPath;
}
/***********************************************************************//**
	@brief ルートからのクラスパスを生成する
	@param[in] name クラス名
	@return クラスパス
***************************************************************************/
std::shared_ptr<KlassPathNode>
Compiler::createRootKlassPath(const std::string& name) {
  auto klassPath = createNode<KlassPathNode>();
  klassPath->appendPath(Klass::ROOT).appendPath(name);
  return klassPath;
}
/***********************************************************************//**
	@brief クラスパスに追加する
	@param[in] klassPath クラスパス
	@param[in] name クラス名
	@return クラスパス
***************************************************************************/
std::shared_ptr<KlassPathNode>
Compiler::appendKlassPath(std::shared_ptr<KlassPathNode> klassPath, 
                          const std::string& name) {
  klassPath->appendPath(name);
  return klassPath;
}
/***********************************************************************//**
	@brief 
***************************************************************************/
std::shared_ptr<ExprNode>
Compiler::addExpr(std::shared_ptr<ExprNode> lhs, 
                  std::shared_ptr<ExprNode> rhs) {
  return createNode<CallNode>("_add_", lhs, rhs);
}
/***********************************************************************//**
	@brief 
***************************************************************************/
std::shared_ptr<ExprNode>
Compiler::subExpr(std::shared_ptr<ExprNode> lhs, 
                  std::shared_ptr<ExprNode> rhs) {
  return createNode<CallNode>("_sub_", lhs, rhs);
}
/***********************************************************************//**
	@brief 
***************************************************************************/
std::shared_ptr<ExprNode>
Compiler::mulExpr(std::shared_ptr<ExprNode> lhs, 
                  std::shared_ptr<ExprNode> rhs) {
  return createNode<CallNode>("_mul_", lhs, rhs);
}
/***********************************************************************//**
	@brief 
***************************************************************************/
std::shared_ptr<ExprNode>
Compiler::divExpr(std::shared_ptr<ExprNode> lhs, 
                  std::shared_ptr<ExprNode> rhs) {
  return createNode<CallNode>("_div_", lhs, rhs);
}
/***********************************************************************//**
	@brief 
***************************************************************************/
std::shared_ptr<ExprNode>
Compiler::modExpr(std::shared_ptr<ExprNode> lhs, 
                  std::shared_ptr<ExprNode> rhs) {
  return createNode<CallNode>("_mod_", lhs, rhs);
}
/***********************************************************************//**
	@brief 
***************************************************************************/
std::shared_ptr<ExprNode>
Compiler::posExpr(std::shared_ptr<ExprNode> value) {
  return createNode<CallNode>("_pos_", value);
}
/***********************************************************************//**
	@brief 
***************************************************************************/
std::shared_ptr<ExprNode>
Compiler::negExpr(std::shared_ptr<ExprNode> value) {
  return createNode<CallNode>("_neg_", value);
}
/***********************************************************************//**
	@brief 
***************************************************************************/
std::shared_ptr<ListNode>
Compiler::appendChild(std::shared_ptr<ListNode> list, 
                      std::shared_ptr<Node> child) {
  list->appendChild(child);
  return list;
}
/***********************************************************************//**
	$Id$
***************************************************************************/
}
