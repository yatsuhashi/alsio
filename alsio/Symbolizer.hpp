﻿/***********************************************************************//**
	@file
***************************************************************************/
#pragma once

namespace alsio {
/***********************************************************************//**
	@brief シンボライザー
***************************************************************************/
class Symbolizer {
 private:
  typedef std::map<std::string, symbol_t> Index;

 private:
  Index indexes_;
  symbol_t symbol_;

 public:
  Symbolizer();
  virtual ~Symbolizer() = default;

  virtual symbol_t getSymbol(const std::string& label);
  virtual std::string getLabel(symbol_t symbol) const;
};
/***********************************************************************//**
	$Id$
***************************************************************************/
}
