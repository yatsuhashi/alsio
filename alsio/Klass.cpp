﻿/***********************************************************************//**
	@file
***************************************************************************/
#include "alsio/Klass.hpp"

namespace alsio {
const std::string Klass::ROOT("");
const std::string Klass::INT("int");
const std::string Klass::FLOAT("float");
const std::string Klass::BOOL("bool");
const std::string Klass::SYMBOL("Symbol");
const std::string Klass::STRING("String");
/***********************************************************************//**
	@brief コンストラクタ
	@param[in] name クラス名
***************************************************************************/
Klass::Klass(const std::string& name)
  : name_(name)
{
}
/***********************************************************************//**
	@brief 
***************************************************************************/
void Klass::appendChild(std::shared_ptr<Klass> child) {
  assert(children_.find(child->getName()) == children_.end());
  children_[child->getName()] = child;
}
/***********************************************************************//**
	@brief 
***************************************************************************/
std::shared_ptr<Klass> Klass::findKlass(const std::string& name) const {
  auto iter = children_.find(name);
  if(iter != children_.end()) {
    return iter->second;
  }
  if(auto klass = getSuper()) {
    if(auto found = klass->findKlass(name)) {
      return found;
    }
  }
  if(auto klass = getParent()) {
    if(auto found = klass->findKlass(name)) {
      return found;
    }
  }
  return nullptr;
}
/***********************************************************************//**
	$Id$
***************************************************************************/
}
