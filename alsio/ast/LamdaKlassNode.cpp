﻿/***********************************************************************//**
	@file
***************************************************************************/
#include "alsio/ast/LamdaKlassNode.hpp"
#include "alsio/ast/KlassRefNode.hpp"
#include "alsio/ast/ParamListNode.hpp"

namespace alsio {
namespace ast {
/***********************************************************************//**
	@brief コンストラクタ
***************************************************************************/
LamdaKlassNode::LamdaKlassNode(std::shared_ptr<ParamListNode> params, 
                               std::shared_ptr<KlassRefNode> klass)
  : params_(params), 
    klass_(klass)
{
}
/***********************************************************************//**
	@brief 
***************************************************************************/
std::string LamdaKlassNode::toString() const {
  std::ostringstream stream;
  stream << params_->toString();
  if(klass_) {
    stream << "->" << klass_->toString();
  }
  return stream.str();
}
/***********************************************************************//**
	$Id$
***************************************************************************/
}
}
