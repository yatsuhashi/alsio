﻿/***********************************************************************//**
	@file
***************************************************************************/
#pragma once

#include "alsio/ast/Node.hpp"

namespace alsio {
namespace ast {
/***********************************************************************//**
	@brief 変数定義ノード
***************************************************************************/
class DefVarNode
  : public Node
{
 private:
  std::shared_ptr<KlassRefNode> klass_;
  std::string name_;
  std::shared_ptr<ExprNode> value_;

 public:
  DefVarNode(std::shared_ptr<KlassRefNode> klass, 
             const std::string& name, 
             std::shared_ptr<ExprNode> value = nullptr);
  ~DefVarNode() override = default;

 protected:
  void onDump(std::ostream& out, 
              const std::string& indent) const override;
};
/***********************************************************************//**
	$Id$
***************************************************************************/
}
}
