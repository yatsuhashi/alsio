﻿/***********************************************************************//**
	@file
***************************************************************************/
#pragma once

#include "alsio/ast/ExprNode.hpp"

namespace alsio {
namespace ast {
/***********************************************************************//**
	@brief シンボルノード
***************************************************************************/
class SymbolNode
  : public ExprNode
{
  typedef ExprNode super;

 private:
  std::string name_;

 public:
  SymbolNode(const std::string& name) : name_(name) {}
  ~SymbolNode() override = default;

  std::string toString() const override {
    return name_;
  }
};
/***********************************************************************//**
	$Id$
***************************************************************************/
}
}
