﻿/***********************************************************************//**
	@file
***************************************************************************/
#pragma once

#include "alsio/ast/ListNode.hpp"

namespace alsio {
namespace ast {
/***********************************************************************//**
	@brief ブロックノード
***************************************************************************/
class BlockNode
  : public ListNode
{
  typedef ListNode super;

 public:
  BlockNode() = default;
  ~BlockNode() override = default;

 protected:
  void onDump(std::ostream& out, 
              const std::string& indent) const override;
};
/***********************************************************************//**
	$Id$
***************************************************************************/
}
}
