﻿/***********************************************************************//**
	@file
***************************************************************************/
#pragma once

#include "alsio/ast/Node.hpp"

namespace alsio {
namespace ast {
/***********************************************************************//**
	@brief 仮引数ノード
***************************************************************************/
class ParamNode
  : public Node
{
 private:
  std::shared_ptr<KlassRefNode> klass_;
  std::string name_;
  std::shared_ptr<Node> value_;

 public:
  ParamNode(std::shared_ptr<KlassRefNode> klass, 
            const std::string& name = "", 
            std::shared_ptr<Node> value = nullptr);
  ~ParamNode() override = default;

  std::string toString() const override;
};
/***********************************************************************//**
	$Id$
***************************************************************************/
}
}
