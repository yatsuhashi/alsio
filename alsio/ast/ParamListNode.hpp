﻿/***********************************************************************//**
	@file
***************************************************************************/
#pragma once

#include "alsio/ast/Node.hpp"

namespace alsio {
namespace ast {
/***********************************************************************//**
	@brief 仮引数リストノード
***************************************************************************/
class ParamListNode
  : public Node
{
  typedef Node super;

 private:
  std::vector<std::shared_ptr<ParamNode>> params_;

 public:
  ParamListNode() = default;
  ParamListNode(std::shared_ptr<ParamNode> param);
  ~ParamListNode() override = default;

  ParamListNode& appendParam(std::shared_ptr<ParamNode> param);

  std::string toString() const override;
};
/***********************************************************************//**
	$Id$
***************************************************************************/
}
}
