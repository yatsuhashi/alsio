﻿/***********************************************************************//**
	@file
***************************************************************************/
#include "alsio/ast/CallNode.hpp"
#include "alsio/ast/ListNode.hpp"

namespace alsio {
namespace ast {
/***********************************************************************//**
	@brief コンストラクタ
***************************************************************************/
CallNode::CallNode(std::shared_ptr<ExprNode> value, 
                   std::shared_ptr<ListNode> args)
  : value_(value), 
    args_(args)
{
}
/***********************************************************************//**
	@copydoc Node::onDump
***************************************************************************/
void CallNode::onDump(std::ostream& out, 
                      const std::string& indent) const {
  value_->dump(out, indent);
  out << indent << "(" << std::endl;
  args_->dump(out, indent + INDENT);
  out << indent << ")" << std::endl;
}
/***********************************************************************//**
	$Id$
***************************************************************************/
}
}
