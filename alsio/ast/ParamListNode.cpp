﻿/***********************************************************************//**
	@file
***************************************************************************/
#include "alsio/ast/ParamNode.hpp"
#include "alsio/ast/ParamListNode.hpp"

namespace alsio {
namespace ast {
/***********************************************************************//**
	@brief 
***************************************************************************/
ParamListNode::ParamListNode(std::shared_ptr<ParamNode> param) {
  appendParam(param);
}
/***********************************************************************//**
	@brief 仮引数を追加する
***************************************************************************/
ParamListNode&
ParamListNode::appendParam(std::shared_ptr<ParamNode> param) {
  params_.push_back(param);
  return *this;
}
/***********************************************************************//**
	@brief 
***************************************************************************/
std::string ParamListNode::toString() const {
  std::ostringstream stream;
  stream << "(";
  for(size_t i = 0, n = params_.size(); i < n; i++) {
    if(i > 0) {
      stream << ",";
    }
    stream << params_.at(i)->toString();
  }
  stream << ")";
  return stream.str();
}
/***********************************************************************//**
	$Id$
***************************************************************************/
}
}
