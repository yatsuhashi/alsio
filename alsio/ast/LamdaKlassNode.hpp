﻿/***********************************************************************//**
	@file
***************************************************************************/
#pragma once

#include "alsio/ast/KlassRefNode.hpp"

namespace alsio {
namespace ast {
/***********************************************************************//**
	@brief ラムダ型ノード
***************************************************************************/
class LamdaKlassNode
  : public KlassRefNode
{
  typedef KlassRefNode super;

 private:
  std::shared_ptr<ParamListNode> params_;
  std::shared_ptr<KlassRefNode> klass_;

 public:
  LamdaKlassNode(std::shared_ptr<ParamListNode> params, 
                 std::shared_ptr<KlassRefNode> klass = nullptr);
                 
  ~LamdaKlassNode() override = default;

  std::string toString() const override;
};
/***********************************************************************//**
	$Id$
***************************************************************************/
}
}
