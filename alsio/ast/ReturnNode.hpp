﻿/***********************************************************************//**
	@file
***************************************************************************/
#pragma once

#include "alsio/ast/Node.hpp"

namespace alsio {
namespace ast {
/***********************************************************************//**
	@brief 
***************************************************************************/
class ReturnNode
  : public Node
{
  typedef Node super;

 private:
  std::shared_ptr<Node> value_;

 public:
  ReturnNode(std::shared_ptr<Node> value = nullptr);
  ~ReturnNode() = default;

 protected:
  void onDump(std::ostream& out, 
              const std::string& indent) const override;
};
/***********************************************************************//**
	$Id$
***************************************************************************/
}
}
