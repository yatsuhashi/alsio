﻿/***********************************************************************//**
	@file
***************************************************************************/
#pragma once

#include "alsio/ast/Node.hpp"

namespace alsio {
namespace ast {
/***********************************************************************//**
	@brief クラス参照ノード
***************************************************************************/
class KlassRefNode
  : public Node
{
  typedef Node super;

 private:
  std::shared_ptr<Klass> klass_;

 public:
  KlassRefNode() = default;
  ~KlassRefNode() override = default;

  const decltype(klass_)& getKlass() const {
    return klass_;
  }

 protected:
  KlassRefNode& setKlass(const decltype(klass_)& klass) {
    klass_ = klass;
    return *this;
  }
};
/***********************************************************************//**
	$Id$
***************************************************************************/
}
}
