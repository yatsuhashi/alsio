﻿/***********************************************************************//**
	@file
***************************************************************************/
#pragma once

#include "alsio/ast/ExprNode.hpp"
#include "alsio/ast/ListNode.hpp"
#include "alsio/ast/SymbolNode.hpp"

namespace alsio {
namespace ast {
/***********************************************************************//**
	@brief 関数呼び出しノード
***************************************************************************/
class CallNode
  : public ExprNode
{
  typedef ExprNode super;

 private:
  std::shared_ptr<ExprNode> value_;
  std::shared_ptr<ListNode> args_;

 public:
  CallNode(std::shared_ptr<ExprNode> value, 
           std::shared_ptr<ListNode> args);
  ~CallNode() override = default;

  template <class... Args>
  CallNode(const std::string& name, const Args&... args)
    : value_(std::make_shared<SymbolNode>(name)), 
      args_(std::make_shared<ListNode>(args...))
  {}

 protected:
  void onDump(std::ostream& out, 
              const std::string& indent) const override;
};
/***********************************************************************//**
	$Id$
***************************************************************************/
}
}
