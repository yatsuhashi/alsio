﻿/***********************************************************************//**
	@file
***************************************************************************/
#pragma once

#include "alsio/ast/Node.hpp"

namespace alsio {
namespace ast {
/***********************************************************************//**
	@brief 関数ノード
***************************************************************************/
class FuncNode
  : public Node
{
  typedef Node super;

 private:
  std::string name_;
  std::shared_ptr<LamdaNode> lamda_;

 public:
  FuncNode(const std::string& name, 
           std::shared_ptr<LamdaNode> lamda);
  ~FuncNode() override = default;

 protected:
  void onDump(std::ostream& out, 
              const std::string& indent) const override;

  void onDump(std::ostream& out, 
              const std::string& prefix, 
              const std::string& indent) const;
};
/***********************************************************************//**
	$Id$
***************************************************************************/
}
}
