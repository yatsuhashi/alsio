﻿/***********************************************************************//**
	@file
***************************************************************************/
#include "alsio/ast/FuncNode.hpp"
#include "alsio/ast/LamdaNode.hpp"

namespace alsio {
namespace ast {
/***********************************************************************//**
	@brief コンストラクタ
***************************************************************************/
FuncNode::FuncNode(const std::string& name, 
                   std::shared_ptr<LamdaNode> lamda)
  : name_(name), 
    lamda_(lamda)
{
}
/***********************************************************************//**
	@copydoc Node::onDump
***************************************************************************/
void FuncNode::onDump(std::ostream& out, 
                      const std::string& indent) const {
  onDump(out, "func", indent);
}
/***********************************************************************//**
	@brief 
***************************************************************************/
void FuncNode::onDump(std::ostream& out, 
                      const std::string& prefix, 
                      const std::string& indent) const {
  out << indent << prefix << " " << name_ << std::endl;
  lamda_->dump(out, indent + INDENT);
}
/***********************************************************************//**
	$Id$
***************************************************************************/
}
}
