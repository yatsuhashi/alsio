﻿/***********************************************************************//**
	@file
***************************************************************************/
#pragma once

#include "alsio/Source.hpp"

namespace alsio {
namespace ast {
/***********************************************************************//**
	@brief 
***************************************************************************/
class Node {
 protected:
  static const std::string INDENT;

 private:
  Source source_;

 public:
  Node() = default;
  virtual ~Node() = default;

  Source& getSource() {
    return source_;
  }

  virtual void eval(Compiler& compiler) {}

  void dump(std::ostream& out, 
            const std::string& indent = "") const;

  virtual std::string toString() const;

 protected:
  virtual void onDump(std::ostream& out, 
                      const std::string& indent) const;
};
/***********************************************************************//**
	$Id$
***************************************************************************/
}
}
