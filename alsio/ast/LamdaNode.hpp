﻿/***********************************************************************//**
	@file
***************************************************************************/
#pragma once

#include "alsio/ast/ExprNode.hpp"

namespace alsio {
namespace ast {
/***********************************************************************//**
	@brief ラムダ式ノード
***************************************************************************/
class LamdaNode
  : public ExprNode
{
  typedef ExprNode super;

 private:
  std::shared_ptr<LamdaKlassNode> klass_;
  std::shared_ptr<BlockNode> block_;

 public:
  LamdaNode(std::shared_ptr<LamdaKlassNode> klass, 
            std::shared_ptr<BlockNode> block);
  ~LamdaNode() override = default;

 protected:
  void onDump(std::ostream& out, 
              const std::string& indent) const override;
};
/***********************************************************************//**
	$Id$
***************************************************************************/
}
}
