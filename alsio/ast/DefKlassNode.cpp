﻿/***********************************************************************//**
	@file
***************************************************************************/
#include "alsio/ast/BlockNode.hpp"
#include "alsio/ast/DefKlassNode.hpp"

namespace alsio {
namespace ast {
/***********************************************************************//**
	@brief コンストラクタ
	@param[in] name クラス名
	@param[in] block 定義ブロック
***************************************************************************/
DefKlassNode::DefKlassNode(const std::string& name, 
                           std::shared_ptr<BlockNode> block)
  : name_(name), 
    block_(block)
{
}
/***********************************************************************//**
	@copydoc Node::onDump
***************************************************************************/
void DefKlassNode::onDump(std::ostream& out, 
                          const std::string& indent) const {
  out << indent << "class " << name_ << std::endl;
  block_->dump(out, indent);
}
/***********************************************************************//**
	$Id$
***************************************************************************/
}
}
