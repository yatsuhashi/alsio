﻿/***********************************************************************//**
	@file
***************************************************************************/
#include "alsio/ast/DefVarNode.hpp"
#include "alsio/ast/ExprNode.hpp"
#include "alsio/ast/KlassRefNode.hpp"

namespace alsio {
namespace ast {
/***********************************************************************//**
	@brief コンストラクタ
***************************************************************************/
DefVarNode::DefVarNode(std::shared_ptr<KlassRefNode> klass, 
                       const std::string& name, 
                       std::shared_ptr<ExprNode> value)
  : klass_(klass), 
    name_(name), 
    value_(value)
{
}
/***********************************************************************//**
	@copydoc Node::onDump
***************************************************************************/
void DefVarNode::onDump(std::ostream& out, 
                        const std::string& indent) const {
  out << indent << klass_->toString() << " " << name_;
  if(value_) {
    out << " = " << std::endl;
    value_->dump(out, indent + INDENT);
  }
  else {
    out << std::endl;
  }
}
/***********************************************************************//**
	$Id$
***************************************************************************/
}
}
