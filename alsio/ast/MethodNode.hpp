﻿/***********************************************************************//**
	@file
***************************************************************************/
#pragma once

#include "alsio/ast/FuncNode.hpp"

namespace alsio {
namespace ast {
/***********************************************************************//**
	@brief メソッドノード
***************************************************************************/
class MethodNode
  : public FuncNode
{
  typedef FuncNode super;

 public:
  MethodNode(const std::string& name, 
             std::shared_ptr<LamdaNode> lamda);
  ~MethodNode() override = default;

 protected:
  void onDump(std::ostream& out, 
              const std::string& indent) const override;
};
/***********************************************************************//**
	$Id$
***************************************************************************/
}
}
