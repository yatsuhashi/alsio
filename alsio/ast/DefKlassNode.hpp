﻿/***********************************************************************//**
	@file
***************************************************************************/
#pragma once

#include "alsio/ast/Node.hpp"

namespace alsio {
namespace ast {
/***********************************************************************//**
	@brief クラス定義ノード
***************************************************************************/
class DefKlassNode
  : public Node
{
  typedef Node super;

 private:
  std::string name_;
  std::shared_ptr<BlockNode> block_;

 public:
  DefKlassNode(const std::string& name, 
               std::shared_ptr<BlockNode> block);
  ~DefKlassNode() override = default;

 protected:
  void onDump(std::ostream& out, 
              const std::string& indent) const override;
};
/***********************************************************************//**
	$Id$
***************************************************************************/
}
}
