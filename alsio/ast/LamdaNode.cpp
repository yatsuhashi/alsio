﻿/***********************************************************************//**
	@file
***************************************************************************/
#include "alsio/ast/BlockNode.hpp"
#include "alsio/ast/LamdaKlassNode.hpp"
#include "alsio/ast/LamdaNode.hpp"

namespace alsio {
namespace ast {
/***********************************************************************//**
	@brief コンストラクタ
	@param[in] klass ラムダ型
	@param[in] block ブロック
***************************************************************************/
LamdaNode::LamdaNode(std::shared_ptr<LamdaKlassNode> klass, 
                     std::shared_ptr<BlockNode> block)
  : klass_(klass), 
    block_(block)
{
}
/***********************************************************************//**
	@copydoc Node::onDump
***************************************************************************/
void LamdaNode::onDump(std::ostream& out, 
                       const std::string& indent) const {
  klass_->dump(out, indent);
  block_->dump(out, indent);
}
/***********************************************************************//**
	$Id$
***************************************************************************/
}
}
