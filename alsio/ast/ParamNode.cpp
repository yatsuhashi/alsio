﻿/***********************************************************************//**
	@file
***************************************************************************/
#include "alsio/ast/KlassRefNode.hpp"
#include "alsio/ast/ParamNode.hpp"

namespace alsio {
namespace ast {
/***********************************************************************//**
	@brief コンストラクタ
***************************************************************************/
ParamNode::ParamNode(std::shared_ptr<KlassRefNode> klass, 
                     const std::string& name, 
                     std::shared_ptr<Node> value)
  : klass_(klass), 
    name_(name), 
    value_(value)
{
}
/***********************************************************************//**
	@copydoc Node::toString
***************************************************************************/
std::string ParamNode::toString() const {
  std::ostringstream stream;
  stream << klass_->toString();
  if(!name_.empty()) {
    stream << " " << name_;
  }
  return stream.str();
}
/***********************************************************************//**
	$Id$
***************************************************************************/
}
}
