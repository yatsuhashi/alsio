﻿/***********************************************************************//**
	@file
***************************************************************************/
#pragma once

#include "alsio/ast/Node.hpp"

namespace alsio {
namespace ast {
/***********************************************************************//**
	@brief リストノード
***************************************************************************/
class ListNode
  : public Node
{
  typedef Node super;

 private:
  std::vector<std::shared_ptr<Node>> children_;

 public:
  ListNode() = default;
  ~ListNode() override = default;

  template <class... Args>
  ListNode(const Args&... args) {
    appendChild(args...);
  }

  template <class Head, class... Tail>
  void appendChild(const Head& head, const Tail&... tail) {
    children_.push_back(head);
    appendChild(tail...);
  }

  void prependChild(std::shared_ptr<Node> child) {
    children_.insert(children_.begin(), child);
  }

 protected:
  const decltype(children_)& getChildren() const {
    return children_;
  }

  void onDump(std::ostream& out, 
              const std::string& indent) const override;

 private:
  void appendChild() {}
};
/***********************************************************************//**
	$Id$
***************************************************************************/
}
}
