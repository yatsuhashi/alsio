﻿/***********************************************************************//**
	@file
***************************************************************************/
#pragma once

#include "alsio/ast/KlassRefNode.hpp"

namespace alsio {
namespace ast {
/***********************************************************************//**
	@brief クラスパスノード
***************************************************************************/
class KlassPathNode
  : public KlassRefNode
{
 private:
  std::vector<std::string> path_;

 public:
  KlassPathNode() = default;
  ~KlassPathNode() override = default;

  KlassPathNode& appendPath(const std::string& name);

  std::string toString() const override;
};
/***********************************************************************//**
	$Id$
***************************************************************************/
}
}
