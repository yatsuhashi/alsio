﻿/***********************************************************************//**
	@file
***************************************************************************/
#include "alsio/Klass.hpp"
#include "alsio/ast/KlassPathNode.hpp"

namespace alsio {
namespace ast {
/***********************************************************************//**
	@brief 
***************************************************************************/
KlassPathNode& KlassPathNode::appendPath(const std::string& name) {
  path_.push_back(name);
  return *this;
}
/***********************************************************************//**
	@copydoc Node::toString
***************************************************************************/
std::string KlassPathNode::toString() const {
  std::ostringstream stream;
  for(size_t i = 0, n = path_.size(); i < n; i++) {
    if(i > 0) {
      stream << "::";
    }
    stream << path_.at(i);
  }
  return stream.str();
}
/***********************************************************************//**
	$Id$
***************************************************************************/
}
}
