﻿/***********************************************************************//**
	@file
***************************************************************************/
#pragma once

#include "alsio/ast/Node.hpp"

namespace alsio {
namespace ast {
/***********************************************************************//**
	@brief 
***************************************************************************/
class ExprNode
  : public Node
{
  typedef Node super;

 private:
  std::shared_ptr<Klass> klass_;

 public:
  ExprNode() = default;
  ~ExprNode() override = default;

  ExprNode& setKlass(std::shared_ptr<Klass> klass) {
    klass_ = klass;
    return *this;
  }

  std::shared_ptr<Klass> getKlass() const {
    return klass_;
  }
};
/***********************************************************************//**
	$Id$
***************************************************************************/
}
}
