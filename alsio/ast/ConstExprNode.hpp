﻿/***********************************************************************//**
	@file
***************************************************************************/
#pragma once

#include "alsio/Variant.hpp"
#include "alsio/ast/ExprNode.hpp"

namespace alsio {
namespace ast {
/***********************************************************************//**
	@brief 定数式ノード
***************************************************************************/
class ConstExprNode
  : public ExprNode
{
  typedef ExprNode super;

 private:
  Variant value_;

 public:
  ConstExprNode(const Variant& value) : value_(value) {}
  ~ConstExprNode() override = default;

  const Variant& getValue() const {
    return value_;
  }

  std::string toString() const override;
};
/***********************************************************************//**
	$Id$
***************************************************************************/
}
}
