﻿/***********************************************************************//**
	@file
***************************************************************************/
#pragma once

namespace alsio {
/***********************************************************************//**
	@brief ソース情報
***************************************************************************/
class Source {
 private:
  const char* filename_;
  int line_;

 public:
  Source();
  Source(const Source& src) = default;
  ~Source() = default;

  Source& setFilename(const char* filename) {
    filename_ = filename;
    return *this;
  }

  const char* getFilename() const {
    return filename_;
  }

  Source& setLine(int line) {
    line_ = line;
    return *this;
  }

  int getLine() const {
    return line_;
  }

  std::string toString() const;
};
/***********************************************************************//**
	$Id$
***************************************************************************/
}
