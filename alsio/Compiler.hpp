﻿/***********************************************************************//**
	@file
***************************************************************************/
#pragma once

#include "alsio/Source.hpp"

namespace alsio {
/***********************************************************************//**
	@brief コンパイラー
***************************************************************************/
class Compiler {
 private:
  std::unique_ptr<Scanner> scanner_;
  std::unique_ptr<Symbolizer> symbolizer_;
  std::shared_ptr<Klass> rootKlass_;
  Source source_;

 public:
  Compiler();
  ~Compiler();

  void compile();

  Symbolizer& getSymbolizer() const {
    return *symbolizer_;
  }

  std::shared_ptr<Klass> findKlass(symbol_t symbol) const;

  void syntax_error();
  void stack_overflow();

  template <class T>
  void downcast(std::shared_ptr<T>& dst, const std::shared_ptr<Node>& src) {
    if(src) {
      dst = std::dynamic_pointer_cast<T>(src);
      assert(dst);
    }
    else {
      dst.reset();
    }
  }
  void downcast(std::string& dst, const std::shared_ptr<Node>& src);

  template <class T>
  void upcast(std::shared_ptr<Node>& dst, const std::shared_ptr<T>& src) {
    dst = src;
  }

  template <class T = Node>
  std::shared_ptr<T> identity(std::shared_ptr<Node> value) {
    assert(std::dynamic_pointer_cast<T>(value));
    return std::static_pointer_cast<T>(value);
  }

  template <class T, class... Args>
  std::shared_ptr<T> createNode(const Args&... args) {
    auto node = std::make_shared<T>(args...);
    node->getSource() = source_;
    return node;
  }

  template <class T>
  std::shared_ptr<T> nullNode() {
    return nullptr;
  }

  std::shared_ptr<BlockNode>
  appendStatement(std::shared_ptr<BlockNode> block, 
                  std::shared_ptr<Node> statement);

  std::shared_ptr<ParamListNode>
  appendParam(std::shared_ptr<ParamListNode> params, 
              std::shared_ptr<ParamNode> param);

  std::shared_ptr<KlassPathNode>
  createKlassPath(const std::string& name);

  std::shared_ptr<KlassPathNode>
  createRootKlassPath(const std::string& name);

  std::shared_ptr<KlassPathNode>
  appendKlassPath(std::shared_ptr<KlassPathNode> klassPath, 
                  const std::string& name);

  std::shared_ptr<ExprNode> addExpr(std::shared_ptr<ExprNode> lhs, 
                                    std::shared_ptr<ExprNode> rhs);
  std::shared_ptr<ExprNode> subExpr(std::shared_ptr<ExprNode> lhs, 
                                    std::shared_ptr<ExprNode> rhs);

  std::shared_ptr<ExprNode> mulExpr(std::shared_ptr<ExprNode> lhs, 
                                    std::shared_ptr<ExprNode> rhs);
  std::shared_ptr<ExprNode> divExpr(std::shared_ptr<ExprNode> lhs, 
                                    std::shared_ptr<ExprNode> rhs);
  std::shared_ptr<ExprNode> modExpr(std::shared_ptr<ExprNode> lhs, 
                                    std::shared_ptr<ExprNode> rhs);

  std::shared_ptr<ExprNode> posExpr(std::shared_ptr<ExprNode> value);
  std::shared_ptr<ExprNode> negExpr(std::shared_ptr<ExprNode> value);

  std::shared_ptr<ListNode> appendChild(std::shared_ptr<ListNode> list, 
                                        std::shared_ptr<Node> child);
};
/***********************************************************************//**
	$Id$
***************************************************************************/
}
