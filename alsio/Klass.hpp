﻿/***********************************************************************//**
	@file
***************************************************************************/
#pragma once

namespace alsio {
/***********************************************************************//**
	@brief クラス
***************************************************************************/
class Klass {
 public:
  static const std::string ROOT;
  static const std::string INT;
  static const std::string FLOAT;
  static const std::string BOOL;
  static const std::string SYMBOL;
  static const std::string STRING;

 private:
  std::string name_;
  std::weak_ptr<Klass> parent_;
  std::weak_ptr<Klass> super_;
  std::map<std::string, std::shared_ptr<Klass>> children_;

 public:
  Klass(const std::string& name);
  ~Klass() = default;

  const std::string& getName() const {
    return name_;
  }

  std::shared_ptr<Klass> getParent() const {
    return parent_.lock();
  }

  std::shared_ptr<Klass> getSuper() const {
    return super_.lock();
  }

  void appendChild(std::shared_ptr<Klass> child);

  std::shared_ptr<Klass> findKlass(const std::string& name) const;
};
/***********************************************************************//**
	$Id$
***************************************************************************/
}
