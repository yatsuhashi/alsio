﻿/***********************************************************************//**
	@file
***************************************************************************/
#include <assert.h>

#include <algorithm>
#include <iostream>
#include <map>
#include <memory>
#include <sstream>
#include <vector>
/***********************************************************************//**
	@brief 
***************************************************************************/
namespace alsio {
typedef int32_t int_t;
typedef float float_t;
typedef uint32_t symbol_t;
}
/***********************************************************************//**
	@brief 
***************************************************************************/
namespace alsio {
class Compiler;
class Klass;
class Scanner;
class Source;
class Symbolizer;
class Variant;
class Value;

namespace ast {
class BlockNode;
class CallNode;
class ConstExprNode;
class DefKlassNode;
class DefVarNode;
class ExprNode;
class FuncNode;
class KlassPathNode;
class KlassRefNode;
class LamdaKlassNode;
class LamdaNode;
class ListNode;
class MethodNode;
class Node;
class ParamNode;
class ParamListNode;
class StatementNode;
class SymbolNode;
}
using namespace ast;
}
/***********************************************************************//**
	$Id$
***************************************************************************/
